# opt-python-rpm
The `build` script will install all Python versions listed in `versions.in` under `/opt` (e.g. `/opt/Python-X.Y`) and package them as RPM using the fpm (https://github.com/jordansissel/fpm) tool. The Python installations and RPM contain the core Python interpreter and libraries plus all PyPi packages (https://pypi.python.org) listed in `pypackages`. 

The main motiviation behind this is to create separate Python installations for testing with Tox (https://testrun.org/tox/latest/). 

Edit the `build` script if you only want the Python installations under `/opt` but don't require the the RPM packages.

