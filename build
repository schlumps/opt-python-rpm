#
# 
#

# PYVERSION must contain a Python version that is available
# as https://www.python.org/ftp/python/<PYVERSION>/Python-<PYVERSION>.tgz
#
# Don't include multiple patch-levels of the same version (e.g. 2.7.8 and 2.7.9)
# as only the MAJOR.MINOR version numbers will be used.
# 
PYVERSIONS=`cat versions.in`

RPM_VERSION=1.0
RPM_RELEASE=1
RPM_CATEGORY="Development/Languages"
RPM_MAINTAINER="Markus Juenemann <markus@juenemann.net>" 

# ------------------------------------------
#

PYPACKAGES=`pwd`/pypackages.in
PYCHANGELOG=`pwd`/PYCHANGELOG
THISDIR=`pwd`

# Install requirements
#
#yum -y install yum-utils rubygems
yum -y groupinstall 'Development tools'
yum -y install `cat packages.in`
yum -y install rubygem-fpm 2>/dev/null	# may not be available!



for PYVERSION in $PYVERSIONS; do

    PYVERSION_SHORT=`echo $PYVERSION | cut -c 1-3`
    PYVERSION_VERYSHORT=`echo $PYVERSION | cut -c 1`
    PYVERSION_BRIEF=`echo $PYVERSION_SHORT | sed 's/\.//g'`

    # Cleanup previously installed RPM
    #
    rpm -q python$PYVERSION_BRIEF-opt && yum -y erase python$PYVERSION_BRIEF-opt


    # Cleanup previous sources and installations
    #
    cd /usr/local/src
    [ -f Python-$PYVERSION.tgz ] && rm -f Python-$PYVERSION.tgz
    [ -d Python-$PYVERSION ] && rm -rf Python-$PYVERSION
    [ -d /opt/Python-$PYVERSION_SHORT ] && rm -rf /opt/Python-$PYVERSION_SHORT


    # Download and extract the Python sources
    # 
    [ -f Python-$PYVERSION.tgz ] || wget https://www.python.org/ftp/python/$PYVERSION/Python-$PYVERSION.tgz
    tar xvfz Python-$PYVERSION.tgz


    # Configure, build and install Python
    #
    cd Python-$PYVERSION
    ./configure --config-cache --prefix=/opt/Python-$PYVERSION_SHORT
    make clean
    make 
    make install


    # Install pip
    #
    cd /opt/Python-$PYVERSION_SHORT/bin
    #RANDFILE=/bin/sh wget --no-check-certificate https://bootstrap.pypa.io/get-pip.py
    wget --no-check-certificate https://bootstrap.pypa.io/get-pip.py
    /opt/Python-$PYVERSION_SHORT/bin/python$PYVERSION_VERYSHORT /opt/Python-$PYVERSION_SHORT/bin/get-pip.py
    [ -f /opt/Python-$PYVERSION_SHORT/bin/pip3 ] && ln -s /opt/Python-$PYVERSION_SHORT/bin/pip3 /opt/Python-$PYVERSION_SHORT/bin/pip


    # Install additional Python packages from PyPi
    #
    for p in `cat $PYPACKAGES`; do
        /opt/Python-$PYVERSION_SHORT/bin/pip install $p --allow-external $p --allow-unverified $p
    done


    # Cleanup *.pyc and *.pyo files
    #
    find /opt/Python-$PYVERSION_SHORT -name "*.pyo" -exec rm -f {} \;
    find /opt/Python-$PYVERSION_SHORT -name "*.pyc" -exec rm -f {} \;


    # Hardlink
    #  
    hardlink -cvv /opt/Python-*

  
    # Create the RPM if fpm is available
    #
    cd $THISDIR
    which fpm >/dev/null 2>&1
    if [ $? -eq 0 ]; then
        fpm -s dir -t rpm -n python$PYVERSION_BRIEF-opt \
            --force \
            --iteration $RPM_RELEASE \
            --version $RPM_VERSION \
            --license Python \
            --category "$RPM_CATEGORY" \
            --maintainer "$RPM_MAINTAINER" \
            --url 'http://www.python.org' \
            --description "Python $PYVERSION_SHORT plus additional packages installed under /opt/Python-$PYVERSION_SHORT.\n\nRun /opt/Python-$PYVERSION_SHORT/bin/pip freeze for a list of packages." \
            --exclude='*.pyc' --exclude='*.pyo' \
            --rpm-changelog $PYCHANGELOG \
            --workdir /tmp \
            /opt/Python-$PYVERSION_SHORT
    fi


    # Cleanup sources
    # 
    rm -fv /usr/local/src/Python-$PYVERSION.tgz
    rm -rfv /usr/local/src/Python-$PYVERSION
done
